<?php /**
 * @file
 * Contains \Drupal\jquery_social_stream\Controller\DefaultController.
 */

namespace Drupal\jquery_social_stream\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\jquery_social_stream\Twitter\TwitterOAuth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Default controller for the jquery_social_stream module.
 */
class DefaultController extends ControllerBase {

  /**
   * Twitter callback for social stream.
   */
  public function jquery_social_stream_twitter_callback() {
    $config = \Drupal::config('jquery_social_stream.settings');

    $consumer_key = $config->get('twitter_api_key');
    $consumer_secret = $config->get('twitter_api_secret');
    $oauth_access_token = $config->get('twitter_access_token');
    $oauth_access_token_secret = $config->get('twitter_access_token_secret');

    switch ($_GET['url']) {
      case 'timeline':
        $rest = 'statuses/user_timeline';
        $params = Array(
          'count' => $_GET['count'],
          'include_rts' => $_GET['include_rts'],
          'exclude_replies' => $_GET['exclude_replies'],
          'screen_name' => $_GET['screen_name']
        );
        break;
      case 'search':
        $rest = "search/tweets";
        $params = array(
          'q' => $_GET['query'],
          'count' => $_GET['count'],
          'include_rts' => $_GET['include_rts']
        );
        break;
      case 'list':
        $rest = "lists/statuses";
        $params = array(
          'list_id' => $_GET['list_id'],
          'count' => $_GET['count'],
          'include_rts' => $_GET['include_rts']
        );
        break;
      default:
        $rest = 'statuses/user_timeline';
        $params = array(
          'count' => '20'
        );
        break;
    }

    $auth = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_access_token, $oauth_access_token_secret);
    $get = $auth->get($rest, $params);

    $output = '';

    if (!$get) {
      $output .= 'An error occurs while reading the feed, please check your connection or settings';
    }
    elseif (isset($get->errors)) {
      foreach ($get->errors as $key => $val) {
        $output .= $val;
      }
    }
    else {
      $output .= $get;
    }

    return new Response($output);
  }

  /**
   * Facebook callback for social stream.
   */
  public function jquery_social_stream_facebook_callback() {
    $config = \Drupal::config('jquery_social_stream.settings');

    $app_id = $config->get('facebook_app_id');
    $app_secret = $config->get('facebook_app_secret');

    // DO NOT EDIT BELOW THIS LINE
    ini_set('display_errors', '0');
    error_reporting(E_ALL | E_STRICT);

    $app_access_token = $app_id . '|' . $app_secret;
    $page_id = isset($_GET['id']) ? $_GET['id'] : '';
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 20;
    $feed = isset($_GET['feed']) ? $_GET['feed'] : 'feed';
    $fields = "id,message,picture,link,name,description,type,icon,created_time,from,object_id,likes,comments";
    $graphUrl = 'https://graph.facebook.com/v2.3/' . $page_id . '/' . $feed . '?key=value&access_token=' . $app_access_token . '&fields=' . $fields;
    $pageUrl = 'https://graph.facebook.com/v2.3/' . $page_id . '?key=value&access_token=' . $app_access_token;

    // get page details
    $pageObject = file_get_contents($pageUrl);

    if ($pageObject === FALSE) {
      $pageObject = self::dc_curl_get_contents($pageUrl);
    }

    $pageDetails = json_decode($pageObject);
    $pageLink = isset($pageDetails->link) ? $pageDetails->link : '';
    $pageName = isset($pageDetails->name) ? $pageDetails->name : '';

    // get page feed
    $graphObject = file_get_contents($graphUrl);

    if ($graphObject === FALSE) {
      $graphObject = self::dc_curl_get_contents($graphUrl);
    }

    $parsedJson = json_decode($graphObject);
    $pagefeed = $parsedJson->data;
    $count = 0;
    $link_url = '';
    $json_decoded = array();

    $json_decoded['responseData']['feed']['link'] = "";
    if (is_array($pagefeed)) {

      foreach ($pagefeed as $data) {

        if (isset($data->message)) {
          $message = $data->message;
        }
        elseif (isset($data->story)) {
          $message = $data->story;
        }
        else {
          $message = '';
        }


        if (isset($data->description)) {
          $message .= ' ' . $data->description;
        }

        $link = isset($data->link) ? $data->link : '';
        $image = isset($data->picture) ? $data->picture : NULL;
        $type = isset($data->type) ? $data->type : '';

        if ($link_url == $link) {
          // continue;
        }

        $link_url = $link;

        if ($type == 'status' && isset($data->story)) {
          continue;
        }

        if (!isset($data->object_id) && $type != 'video') {
          $pic_id = explode("_", $image);
          if (isset($pic_id[1])) {
            $data->object_id = $pic_id[1];
          }
        }

        if (isset($data->object_id)) {

          if (strpos($image, 'safe_image.php') === FALSE && is_numeric($data->object_id)) {
            $image = 'http://graph.facebook.com/' . $data->object_id . '/picture?type=normal';
          }

        }

        $json_decoded['responseData']['feed']['entries'][$count]['pageLink'] = $pageLink;
        $json_decoded['responseData']['feed']['entries'][$count]['pageName'] = $pageName;
        $json_decoded['responseData']['feed']['entries'][$count]['link'] = $link;
        $json_decoded['responseData']['feed']['entries'][$count]['content'] = $message;
        $json_decoded['responseData']['feed']['entries'][$count]['thumb'] = $image;
        $json_decoded['responseData']['feed']['entries'][$count]['publishedDate'] = date("D, d M Y H:i:s O", strtotime($data->created_time));
        $count++;
      }
    }

    header("Content-Type: application/json; charset=UTF-8");
    return new Response(drupal_json_encode($json_decoded));
  }

  protected static function dc_curl_get_contents($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
  }

}
